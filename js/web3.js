(function ($, Drupal, window, document) {
  // Only continue if web3 is enabled.
  if (window.web3) {

    $('#connect-wallet').click(function () {
      connectWallet();
    });

    $('#disconnect-wallet').click(function () {
      disconnectWallet();
    });

    // Standalone wbe3modal.
    const Web3Modal = window.Web3Modal.default;
    const WalletConnectProvider = window.WalletConnectProvider.default;
    const ethers = window.ethers;

    console.log("WalletConnectProvider is", WalletConnectProvider);
    console.log("window.web3 is", window.web3);
    console.log("window.ethereum is", window.ethereum);

    // By default Metamask is used.
    const providerOptions = {}

    const web3Modal = new Web3Modal({
      // network: "mainnet", // optional
      cacheProvider: false, // optional
      providerOptions // required
    });

    console.log("Web3Modal instance is", web3Modal);

    onLoad();

    async function onLoad() {

      // Get user id.
      var uid = drupalSettings.user.uid;

      // Check if we're still logged into Drupal, then make sure still
      // connected to wallet.
      // We skip super admin user.
      if (uid !== 0 && uid !== 1) {
        console.log('User logged in... initialise wallet connection...')

        try {
          const provider = await web3Modal.connect();
          console.log("Provider: ", provider);

          // Subscribe to accounts change
          provider.on("accountsChanged", (accounts) => {
            console.log("accountsChanged: ", accounts);
            web3Modal.clearCachedProvider();

            if (accounts.length === 0) {
              console.log('User disconnected...');
              console.log("Drupal logout...");
              var data = $.parseJSON($.ajax({
                url: Drupal.url("web3/logout"),
                type: "GET",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false
              }).responseText);

              window.location.reload(false);
            }
          });

          // Subscribe to chainId change
          provider.on("chainChanged", (chainId) => {
            console.log("chainChanged: ", chainId);
          });

          // Subscribe to provider connection
          provider.on("connect", (chainId) => {
            console.log("connect: ", chainId);
          });

          // Subscribe to session disconnection
          provider.on("disconnect", (code, reason) => {
            console.log("disconnect: ", code, reason);
          });

          const web3ethers = new ethers.providers.Web3Provider(provider);

          // Fill in the Wallet Info block
          // @todo - extract nicely into a function

          console.log("Balance: ", web3ethers.getSigner().getBalance());
          balance = await web3ethers.getSigner().getBalance();
          balance = ethers.utils.formatEther(balance)
          $('span.wallet-balance').text(balance);

          // https://chainlist.org for list of chains.
          chainId = await web3ethers.getSigner().getChainId();
          $('span.wallet-chain').text(chainId);

        } catch (e) {
          console.log("Closed connection!");
          console.log("Drupal logout...");

          var data = $.parseJSON($.ajax({
            url: Drupal.url("web3/logout"),
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
          }).responseText);
          window.location.reload(false);

        }
      } else {
        // If we're not logged into Drupal then no need to do anything,
        // we simply wait for user to press 'Connect Wallet'.
        console.log('User not logged in...')
      }
    }

    async function connectWallet() {
      // @todo - wrap in try/catch here too?
      const provider = await web3Modal.connect();
      console.log("Provider: ", provider);

      const web3ethers = new ethers.providers.Web3Provider(provider);
      console.log("Wallet address: ", web3ethers.getSigner().getAddress());

      var data = $.parseJSON($.ajax({
        url: Drupal.url("web3/signature"),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
      }).responseText);

      // @todo - error checks?
      signature = data[0];
      console.log("Signature: ", signature);

      data = $.parseJSON($.ajax({
        url: Drupal.url("web3/login"),
        type: "POST",
        data: {
          "address": await web3ethers.getSigner().getAddress(),
          "signature": await web3ethers.getSigner().signMessage(signature),
        },
        async: false
      }).responseText);
      console.log("Drupal Login: ", data);
      window.location.reload(false);
    }

    async function disconnectWallet() {
      console.log('Disconnect wallet...');

      // It's really up to the user to disconnect a site from their wallet,
      // so it can only be 'faked' from our point of view.
      provider = await web3Modal.connect();
      if (provider.close) {
        await provider.close();
        await web3Modal.clearCachedProvider();

        provider = null;
      }

      // By logging the user out from drupal we at least get rid of the session
      // and force the user to reconnect the wallet if they wish to.
      console.log("Drupal logout...");
      var data = $.parseJSON($.ajax({
        url: Drupal.url("web3/logout"),
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false
      }).responseText);

      window.location.reload(false);
    }

  }

}(jQuery, Drupal, window, drupalSettings));
