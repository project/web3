## SUMMARY
Module provides ability for users to login into Drupal using their Web3 Wallet.

If the user never logged into Drupal before then a new user is created for
them, and they then get logged in automatically.

A user has the ability to 'disconnect' their wallet, and they get logged out of
Drupal, however a user has to actually disconnect from a site from their wallet
to be fully disconnected.

Once module is enabled the ConnectWalletBlock needs to be added somewhere on
the site, for example on the Secondary Menu region where the old 'User account
menu' was. Ideally the old 'User account menu' should be removed to avoid
confusion.

The 'normal' user routes are being redirected back to the homepage since they
shouldn't be used directly.

To login as admin you can still access the old user login form by adding the
query parameter to the url (similar to how OpenID Connect module does it):
`/user/login?showlogin`

##TODO
- add more wallet integrations just as Fortmatic, etc.
- change the wallet provider from MetaMask to Wallet Connect
- look at providing multiple blockhain integrations depending on preference?
  - Solana, Avalanche, etc.
  - can this actually be done?
  - https://solana-labs.github.io/solana-web3.js/
- disable 'Connect Wallet' button after being clicked it, so it can't be
clicked again
- handle when user logged out of their wallet, and they don't actually login
  - in that case you're still logged into drupal, but the user doesn't complete
  login on wallet...
  - seems to also break the 'disconnect' button, because you're technically
  not logged into wallet
    - RPC Error: Already processing eth_requestAccounts. Please wait.
- test and handle changing wallet account:
  - ie. metamask with multiple accounts
  - that should in theory be handled and switch to a different drupal user?

##Resources
Module inspired from a few other PHP libraries:

https://github.com/m1guelpf/laravel-web3-login

https://github.com/giekaton/php-metamask-user-login

Module uses the following JS libraries to create the Web3 connections:

https://github.com/Web3Modal/web3modal

https://github.com/ethers-io/ethers.js
