<?php

namespace Drupal\web3\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * This block is provided mostly as an example.
 *
 * The information extracted such as balance might be something that needs
 * to be obfuscated, or at least provided the user with a simple button to
 * obfuscate it if they choose to.
 *
 * @Block(
 *   id = "wallet_info_block",
 *   admin_label = @Translation("Wallet Info Block"),
 * )
 */
class WalletInfoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $current_user = \Drupal::currentUser();
    $logged_in = $current_user->isAuthenticated();

    if ($logged_in) {
      $user = User::load($current_user->id());
      $wallet_address = $user->field_wallet_address->value;
      $wallet_address = substr($wallet_address, 0, 5) . '...' . substr($wallet_address, -4);

      $markup = $this->t('
        <p>Wallet address: <span class="wallet-address"> %wallet_address</span></p>
        <p>Chain: <span class="wallet-chain"></span></p>
        <p>Balance: <span class="wallet-balance"></span></p>
      ', ['%wallet_address' => $wallet_address]);
    }
    else {
      $markup = $this->t('<p>No wallet connected.</p>');
    }

    return [
      '#markup' => $markup,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['wallet_info_settings'] = $form_state->getValue('wallet_info_settings');
  }

}
